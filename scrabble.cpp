#include <iostream>
#include <vector>
#include <chrono>
#include <algorithm>
#include <fstream>
#include <map>



size_t utf8Len(std::string s);

// Funkcja, ktora rekurencyjnie wyciaga wszystkie podciagi slowa word
void getSubWordsRecursive(const std::string& word, const std::string& midword, int dependency, int depth, std::vector<std::string>& vec);

// Funkcja wyciagajaca wszystkie podciagi slowa word
std::vector<std::string> getSubWords(const std::string& word);



int main(){

    std::ifstream input("slowa.txt");
    std::string line;

    std::multimap<std::string,std::string> dict;

    while (std::getline(input, line)){
        line.erase(std::remove(line.begin(), line.end(), '\r'), line.end());
        if (line.size() == utf8Len(line)){
           std::string normalLine = line;
           std::sort(line.begin(), line.end());
           dict.insert(std::make_pair(line, normalLine));
        } 
    }



    std::string word;
    std::string letter;

    std::cout << "Podaj 1 literę, do której chcesz dołożyć wyraz: ";
    std::cin >> letter;
    std::cout << "Podaj 7 liter, które masz: ";
    std::cin >> word;
    std::cout<<std::endl;
    

    std::vector<std::string> keys = getSubWords(word);
    std::vector<std::string> result;

    for (auto& key : keys){
        // Zwraca parę iteratorów na wartosci spod danego klucza (pierwszy iterator wskazuje na poczatek, drugi na koniec)
        key += letter;
        sort(key.begin(), key.end());
        auto range = dict.equal_range(key); 
        for (auto i = range.first; i != range.second; ++i)
        {
            result.push_back(i->second);
        }
    }
    std::sort(result.begin(), result.end(), [](const std::string& s1, const std::string& s2){
        return s1.size() > s2.size();
    });
 
    for (const std::string& str: result){
        std::cout << str << std::endl;
    }
    return 0;
}



size_t utf8Len(std::string s){
     
    return std::count_if(s.begin(), s.end(), [](char c){return ((static_cast<unsigned char>(c) & 0xC0) != 0x80);});
}



std::vector<std::string> getSubWords(const std::string& word){

    std::vector<std::string> subWords;

    for (int k = 0; k < word.size(); ++k){
            std::string w(word.begin()+k, word.begin()+k+1);
            getSubWordsRecursive(word, w, k, 1, subWords);
    }

    // Sortowanie liter w kazdym slowie z wektora
    for (std::string& str: subWords){
        std::sort(str.begin(),str.end());
    }

    // Sortowanie slow w wektorze
    std::sort(subWords.begin(),subWords.end());
    
    //Usuwanie duplikatow slow
    subWords.erase(std::unique(subWords.begin(),subWords.end()), subWords.end());
    return subWords;
}



void getSubWordsRecursive(const std::string& word, const std::string& prevSubWord, int dependency, int depth, std::vector<std::string>& vec){

    for (int i = dependency+1; i < word.size(); ++i){
            std::string subWord = prevSubWord;
            subWord.insert(depth, std::string(word.begin()+i, word.begin()+i+1));
            vec.push_back(subWord);
            getSubWordsRecursive(word, subWord, i, depth+1, vec);
    }
}
